var Cart = {
    vars:{
        cart:JSON.parse(localStorage.getItem('cart')),
    },
    add(product){
        console.log(product);
        let cart = this.vars.cart;

        if(cart.length != 0){
            let add = true;
            for (let i=0;cart.length > i;i++){
                  let item = cart[i];
                  if(item.id == product.id){
                    item.units = item.units + product.units;
                    add = false;
                    break;
                  }
            }  
            if(add){
              cart.push(product);
            }      
          }else{
            cart.push(product);
          }  

        localStorage.setItem('cart',JSON.stringify(cart));
        
        this.createTable();
    },
    remove(){

    },
    delete(){

    },
    createTable(){
        let cart = this.vars.cart;
        let table = $("#tBodyShopCart");
        let productos = $("#products");
        let total = $("#total");
        let tbody = "";
        let total_units = 0;
        let total_amount = 0;
  
        if(cart.length > 0){
          for (let i=0;cart.length > i;i++){
          let item = cart[i];
          tbody += "<tr>";
          
          tbody += "<td>"+item.units+"</td>";
          tbody += "<td>x</td>";
          tbody += "<td>"+item.name+"</td>";
          tbody += "<td>$ "+item.price+"</td>";
          tbody += "<td>=</td>";
          tbody += "<td>$ "+(item.price * item.units).toFixed(2)+"</td>";
          tbody += '<td>'+
                    '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></button>'+
                    '<div class="dropdown-menu" aria-labelledby="navbarDropdown">'+
                     '<button class="dropdown-item btnAdd" id-product="'+item.id+'"><i class="fa fa-plus"></i> Agregar</button></li>'+
                     '<button class="dropdown-item btnRemove" id-product="'+item.id+'"><i class="fa fa-minus"></i> Quitar</button></li>'+
                     '<button class="dropdown-item btnDelete" id-product="'+item.id+'"><i class="fa fa-trash"></i> Eliminar</button></li>'+
                    '</div>'+
                    '</td>';
  
          tbody += "</tr>";
  
          total_units = total_units + item.units;
          total_amount = (total_amount + (item.price * item.units));
  
          } 
  
          table.html(tbody);
          productos.html("Productos: " + total_units);
          total.html("Total: $ " + total_amount.toFixed(2) + " MXN");
        }else{
          let table = $("#tBodyShopCart");
          let productos = $("#products");
          let total = $("#total");
          let tbody = "";
  
          tbody += "<tr>";
  
          tbody += "<td class='text-center' colspan='7'>";
          tbody += "Aún no tienes productos agregados";
          tbody += "</td>";
          tbody += "</tr>";
  
          table.html(tbody);
          productos.html("Productos: 0");
          total.html("Total: $ 0.00 MXN");
  
        }
        
      }
    
} 