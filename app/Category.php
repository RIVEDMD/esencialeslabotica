<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $table="categories";
    protected  $primaryKey="id";

    protected $fillable = [
        'category','brand_id','description','image'
    ];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
}