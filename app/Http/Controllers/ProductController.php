<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function seeProduct($id)
    {
        //enviar datos del producto
        $product = Product::find($id);
        $also_bought = Product::all();

        return view('principal.product')->with(["product"=>$product,"also_bought"=>$also_bought]);
    }

}
