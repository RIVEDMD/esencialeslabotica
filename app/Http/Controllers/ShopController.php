<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Brand;
use App\Category;
use App\Product;

class ShopController extends Controller
{
    public function main()
    {
        return view('principal.shop');
    }

    public function brands()
    {
        $brands = Brand::all();

        return view('principal.brand')->with("brands",$brands);
    }

    public function brand_categories($id)
    {
        $brand = Brand::find($id);

        return view('principal.category')->with("brand",$brand);
    }

    public function category_products($id)
    {
        $products = Product::all();

        return view('principal.products-grid')->with("products",$products);
    }
}
