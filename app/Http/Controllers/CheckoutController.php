<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CheckoutController extends Controller
{
    public function seeCheckOut(Request $r)
    {
        //enviar datos del producto
        
        $shopItems = json_decode($r);
        $products = [];
        $total = 0;
        $total_articulos = 0;

        foreach($shopItems as $item){

            $product = Product::find($item->id);

            if($product){
                $data = ["unidades"=>$item->units,
                        "producto"=>$product->product,
                        "imagen"=>json_decode($product->images)[1],
                        "precio"=>$product->price,
                        "importe"=>$product->price * $item->units];

                array_push($products,$data);
                $total = $total + $data["importe"];
                $total_articulos = $total_articulos + $data["unidades"];
            }
        }

        return view('principal.checkout')->with(["products"=>$products,"total"=>$total,"total_articulos"=>$total_articulos]);
    }

}
