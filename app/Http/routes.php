<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('layouts.main');
    return redirect("/home");
});

Route::get('home','HomeController@main');

Route::get('shop', 'ShopController@main');
Route::get('shop/brands', 'ShopController@brands');
Route::get('shop/brand/categories/{id}', 'ShopController@brand_categories');
Route::get('shop/brand/category/products/{id}', 'ShopController@category_products');

Route::get('product/{id}', 'ProductController@seeProduct');

Route::get('checkout/', 'CheckoutController@seeCheckOut');


