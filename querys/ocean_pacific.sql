-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2020 a las 04:32:06
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ocean_pacific`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `brands`
--

INSERT INTO `brands` (`id`, `brand`, `description`, `image`) VALUES
(1, 'Esenciales La Botica', 'Aceites esenciales naturales', 'https://www.iwachem.com/wp-content/uploads/2015/09/LOGO-IWACHEM.png'),
(3, 'Iwa Chem', 'Productos sanitizantes de origen natural', 'https://www.iwachem.com/wp-content/uploads/2015/09/LOGO-IWACHEM.png'),
(4, 'iCompost', 'Productos desechables Biodegradables', 'https://www.iwachem.com/wp-content/uploads/2015/09/LOGO-IWACHEM.png'),
(5, 'Otro', 'Otro', 'https://www.iwachem.com/wp-content/uploads/2015/09/LOGO-IWACHEM.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `category`, `brand_id`, `description`, `image`) VALUES
(1, 'Concentrados', 3, 'Concentrados de sanitizante', 'https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870'),
(2, 'Todo Uso', 3, 'Para todo uso', 'https://cdn.shopify.com/s/files/1/3007/6178/products/5_2260320222756.jpg?v=1597706870');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories_products`
--

CREATE TABLE `categories_products` (
  `id_catetegory_product` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories_products`
--

INSERT INTO `categories_products` (`id_catetegory_product`, `id_category`, `id_product`) VALUES
(1, 2, 1),
(2, 2, 3),
(3, 2, 4),
(4, 2, 5),
(5, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id_client` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `direction` varchar(500) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id_order` int(11) NOT NULL,
  `specification` varchar(1000) DEFAULT NULL,
  `id_transaction` int(11) NOT NULL,
  `id_client` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_products`
--

CREATE TABLE `orders_products` (
  `id_order_product` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `units` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product` varchar(100) NOT NULL,
  `tiny_description` varchar(100) DEFAULT '',
  `features` text DEFAULT NULL,
  `description` varchar(500) NOT NULL,
  `specs` text DEFAULT NULL,
  `price` float NOT NULL,
  `units` int(11) DEFAULT NULL,
  `images` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sold_units` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `product`, `tiny_description`, `features`, `description`, `specs`, `price`, `units`, `images`, `created_at`, `updated_at`, `sold_units`) VALUES
(1, 'Tane Citrus', 'Sanitizante Natural Cítrico', '[{\"title\":\"Contenido\",\"def\":\"1 L\"},{\"title\":\"Marca\",\"def\":\"TANE\"},{\"title\":\"Producto\",\"def\":\"Sanitizante\"},{\"title\":\"Color\",\"def\":\"Mandarina\"},{\"title\":\"Olor\",\"def\":\"Cítrico\"}]', 'Sanitizante natural a base de extracto de semillas de cítricos', '[\"Atomizar directo en la superficie al limpiar\",\"No es toxico\",\"Si, es Biodegradable\"]', 120, 10, '[\"https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870\",\"https://cdn.shopify.com/s/files/1/3007/6178/products/5_2260320222756.jpg?v=1597706870\"]', '2020-08-17 19:46:35', NULL, NULL),
(2, 'Tane Citrus Concentrado', 'Sanitizante Natural Cítrico', '[{\"title\":\"Contenido\",\"def\":\"1 L\"},{\"title\":\"Marca\",\"def\":\"TANE\"},{\"title\":\"Producto\",\"def\":\"Sanitizante\"},{\"title\":\"Color\",\"def\":\"Mandarina\"},{\"title\":\"Olor\",\"def\":\"Cítrico\"}]', 'Sanitizante natural a base de extracto de semillas de cítricos', '[\"Atomizar directo en la superficie al limpiar\",\"No es toxico\",\"Si, es Biodegradable\"]', 1900, 10, '[\"https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870\",\"https://cdn.shopify.com/s/files/1/3007/6178/products/5_2260320222756.jpg?v=1597706870\"]', '2020-08-17 19:46:35', '0000-00-00 00:00:00', NULL),
(3, 'Tane Kumo', 'Sanitizante Natural Cítrico', '[{\"title\":\"Contenido\",\"def\":\"1 L\"},{\"title\":\"Marca\",\"def\":\"TANE\"},{\"title\":\"Producto\",\"def\":\"Sanitizante\"},{\"title\":\"Color\",\"def\":\"Mandarina\"},{\"title\":\"Olor\",\"def\":\"Cítrico\"}]', 'Sanitizante natural a base de extracto de semillas de cítricos', '[\"Atomizar directo en la superficie al limpiar\",\"No es toxico\",\"Si, es Biodegradable\"]', 200, 10, '[\"https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870\",\"https://cdn.shopify.com/s/files/1/3007/6178/products/5_2260320222756.jpg?v=1597706870\"]', '2020-08-17 19:46:35', '0000-00-00 00:00:00', NULL),
(4, 'Tane Citrus 2', 'Sanitizante Natural Cítrico', '[{\"title\":\"Contenido\",\"def\":\"1 L\"},{\"title\":\"Marca\",\"def\":\"TANE\"},{\"title\":\"Producto\",\"def\":\"Sanitizante\"},{\"title\":\"Color\",\"def\":\"Mandarina\"},{\"title\":\"Olor\",\"def\":\"Cítrico\"}]', 'Sanitizante natural a base de extracto de semillas de cítricos', '[\"Atomizar directo en la superficie al limpiar\",\"No es toxico\",\"Si, es Biodegradable\"]', 120, 10, '[\"https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870\",\"https://cdn.shopify.com/s/files/1/3007/6178/products/5_2260320222756.jpg?v=1597706870\"]', '2020-08-17 19:46:35', '0000-00-00 00:00:00', NULL),
(5, 'Tane Citrus 3', 'Sanitizante Natural Cítrico', '[{\"title\":\"Contenido\",\"def\":\"1 L\"},{\"title\":\"Marca\",\"def\":\"TANE\"},{\"title\":\"Producto\",\"def\":\"Sanitizante\"},{\"title\":\"Color\",\"def\":\"Mandarina\"},{\"title\":\"Olor\",\"def\":\"Cítrico\"}]', 'Sanitizante natural a base de extracto de semillas de cítricos', '[\"Atomizar directo en la superficie al limpiar\",\"No es toxico\",\"Si, es Biodegradable\"]', 120, 10, '[\"https://cdn.shopify.com/s/files/1/3007/6178/products/V_17.png?v=1597706870\",\"https://cdn.shopify.com/s/files/1/3007/6178/products/5_2260320222756.jpg?v=1597706870\"]', '2020-08-17 19:46:35', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions`
--

CREATE TABLE `promotions` (
  `id_promotion` int(11) NOT NULL,
  `promotion` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `operator` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotions_products`
--

CREATE TABLE `promotions_products` (
  `id_promotion_product` int(11) NOT NULL,
  `id_promotion` int(11) NOT NULL,
  `id_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategories`
--

CREATE TABLE `subcategories` (
  `id_subcatgory` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `subcategory` varchar(50) NOT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategories_products`
--

CREATE TABLE `subcategories_products` (
  `id_subcategory_product` int(11) NOT NULL,
  `id_subcategory` int(11) NOT NULL,
  `id_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transactions`
--

CREATE TABLE `transactions` (
  `id_transaction` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `pay_at` datetime DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `last_name` int(50) NOT NULL,
  `amount` float NOT NULL,
  `pay` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories_products`
--
ALTER TABLE `categories_products`
  ADD PRIMARY KEY (`id_catetegory_product`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id_client`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_transaction` (`id_transaction`),
  ADD KEY `id_client` (`id_client`);

--
-- Indices de la tabla `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`id_order_product`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id_promotion`);

--
-- Indices de la tabla `promotions_products`
--
ALTER TABLE `promotions_products`
  ADD PRIMARY KEY (`id_promotion_product`);

--
-- Indices de la tabla `subcategories_products`
--
ALTER TABLE `subcategories_products`
  ADD PRIMARY KEY (`id_subcategory_product`);

--
-- Indices de la tabla `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id_transaction`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categories_products`
--
ALTER TABLE `categories_products`
  MODIFY `id_catetegory_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders_products`
--
ALTER TABLE `orders_products`
  MODIFY `id_order_product` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id_promotion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `promotions_products`
--
ALTER TABLE `promotions_products`
  MODIFY `id_promotion_product` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id_transaction` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
