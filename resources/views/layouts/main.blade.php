<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>@yield('title') | Esenciales La Botica</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  @yield('styles')
  <!-- end: Css -->

  <!-- =======================================================
    Theme Name: EstateAgency
    Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <div class="click-closed"></div>
  <!--/ Form Search Star /-->
 
  <!--/ Form Search End /-->

  <!-- ============================================================== -->
    <!-- ********************* Start right Shopping Cart here ****************-->
    <!-- ============================================================== -->
    @include('sources.shoppingcart')
    <!-- ============================================================== -->
    <!-- ********************** End right Shopping Cart here *****************-->
    <!-- ============================================================== -->

  <!-- ============================================================== -->
    <!-- ********************** Start Top Bar **************************-->
    <!-- ============================================================== -->
    @include('sources.topbar')
    <!-- ============================================================== -->
    <!-- *********************** End Top Bar ***************************-->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- ********************* Start right Content here ****************-->
    <!-- ============================================================== -->
    @include('sources.content')
    <!-- ============================================================== -->
    <!-- ********************** End right Content here *****************-->
    <!-- ============================================================== -->


  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('lib/popper/popper.min.js')}}"></script>
  <script src="{{asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('lib/scrollreveal/scrollreveal.min.js')}}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{asset('js/main.js')}}"></script>
  <script src="{{asset('js/shoppingCart.js?v-'.rand())}}"></script>

  <script>
    $(document).ready(function(){
      Cart.createTable();
    });
  </script>

  @yield('scripts')
  
</body>
</html>
