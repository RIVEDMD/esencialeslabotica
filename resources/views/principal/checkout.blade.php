@extends('layouts.main')
@section('title')
Verifica tu compra
@endsection
@section('styles')
  <style>
    .img-product img{
      width: 40px ;
      height: 40px ;
      border-radius: 150px;
    }
    .card-box-cart{
      background: #ffffff;
      width: 100%;
      padding: 20px;
      border-radius: 5px;
      -webkit-box-shadow: -4px 3px 14px -5px rgba(0,0,0,0.68);
      -moz-box-shadow: -4px 3px 14px -5px rgba(0,0,0,0.68);
      box-shadow: -4px 3px 14px -5px rgba(0,0,0,0.68);
    }
  </style>
@endsection
@section('contenido')
<section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">Carrito</h1>
            <span class="color-text-a">Productos: {{count($products)}}</span>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/home">Inicio</a>
              </li>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Carrito
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
<section class="property-single nav-arrow-b">
    <div class="container">
      <div class="row">
        <table class="table table-lg">
          <thead class="table-secondary">
            <tr>
              <td>Cant.</td>
              <td></td>
              <td>Producto</td>
              <td>Precio</td>
              <td></td>
              <td>Importe</td>
              <td>Opciones</td>
            </tr>
          </thead>
          <tbody id="tBodyShopCart"> 
            @foreach ($products as $product)
              <tr>
                <td>{{$product["unidades"]}}</td>
                <td>x</td>
                <td class="d-flex justify-content"><div class="img-product mr-3"><img src="{{$product["imagen"]}}"/></div>{{$product["producto"]}}</td>
                <td>{{$product["precio"]}}</td>
                <td>x</td>
                <td>{{$product["importe"]}}</td>
                <td>Botones</td>
              </tr>
            @endforeach
          </tbody>
        </table> 
        <div class="col-lg-12 mb-3">
          <h3>Total de artículos: {{$total_articulos}}</h3>
          <h3>Total a pagar: $ {{$total}} MXN</h3>
        </div>
        <div class="col-lg-12 card-box-cart">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2>¡Ya casi es tuyo!</h2>
              <p>Completa el formulario y realiza el pago de manera segura.</p>
            </div>
            <div class="col-lg-12 title-box-d ml-3">
              <h3 class="title-d">Datos de contacto</h3>
            </div>
            <div class="col-md-12 mb-3">
              <div class="form-group">
                <input type="text" name="name" class="form-control form-control-lg form-control-a" placeholder="Nombre completo" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <div class="form-group">
                <input name="email" type="email" class="form-control form-control-lg form-control-a" placeholder="Correo electrónico" data-rule="email" data-msg="Please enter a valid email">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <div class="form-group">
                <input name="email" type="email" class="form-control form-control-lg form-control-a" placeholder="Teléfono móvil" data-rule="email" data-msg="Please enter a valid email">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-lg-12 title-box-d ml-3">
              <h3 class="title-d">Datos de contacto</h3>
            </div>
            <div class="col-md-12 mb-3">
              <div class="form-group">
                <input type="url" name="subject" class="form-control form-control-lg form-control-a" placeholder="Dirección completa" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-md-4 mb-3">
              <div class="form-group">
                <input type="url" name="subject" class="form-control form-control-lg form-control-a" placeholder="Estado" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-md-4 mb-3">
              <div class="form-group">
                <input type="url" name="subject" class="form-control form-control-lg form-control-a" placeholder="Municipio" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-md-4 mb-3">
              <div class="form-group">
                <input type="url" name="subject" class="form-control form-control-lg form-control-a" placeholder="CP (código postal)" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                <div class="validation"></div>
              </div>
            </div>
            <div class="col-md-12 mb-3">
              <div class="form-group">
                <textarea name="message" class="form-control" name="message" cols="45" rows="8" data-rule="required" data-msg="" placeholder="Especificaciones"></textarea>
                <div class="validation"></div>
              </div>
            </div>
          </div>
          <div class="col-lg-12 text-center">
            <button class=" text-center btn btn-b col-lg-4">¡Confirmar y realizar el pago!</button>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section("scripts")
  <script src="{{asset('js/shoppingCart.js?v-'.rand())}}"></script>
  <script>
    $(document).ready(function(){
    
    });
  </script>
@endsection
