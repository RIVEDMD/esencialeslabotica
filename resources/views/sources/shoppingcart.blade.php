
<style scoped>
  .agregar{
    border-radius:50px;
  }

  .restar{
    border-radius:50px;
  }

  .eliminar{
    border-radius:50px;
  }
</style>

<div class="click-closed"></div>
  <!--/ Form Search Star /-->
  <div class="box-collapse">
    <div class="title-box-d">
      <h3 class="title-d">Tú Carrito</h3>
    </div>
    <span class="close-box-collapse right-boxed ion-ios-close"></span>

    <div class="col-lg-12">
    <table class="table table-lg">
      <thead class="table-secondary">
        <tr>
          <td>Cant.</td>
          <td></td>
          <td>Producto</td>
          <td>Precio</td>
          <td></td>
          <td>Importe</td>
          <td></td>
        </tr>
      </thead>
      <tbody id="tBodyShopCart"> 
      </tbody>
    </table> 
  <hr>
    <p id="products"></p>
    <p id="total"></p>
    
      <button class=" text-center btn btn-b col-lg-4" id="buy">¡Comprar Ahora!</button>
    </div>
  </div>

  @section('scripts')
  <script src="{{asset('js/shoppingCart.js?v-'.rand())}}"></script>
  <script>
  
    $(document).ready(function(){
      $("#tBodyShopCart").on("click",".btnAdd",function(){
        alert("add");
        let id_product = $(this).attr("id-product");

        Cart.add({id:id_product,units:1});
      });
      
      $("#buy").on("click",function(){
        debugger;

        let products = localStorage.getItem('cart');

        let formData = new FormData();

        formData.append("products",products);

        $.ajax({
          url: '/checkout/',
          type: 'GET',
          cache: false,
          contentType: false,
          processData: false,
          data:formData,
          success: function(data){
              
          }
        });
      });

      $(".btnRemove").on("click",function(){
        let cart = JSON.parse(localStorage.getItem('cart'));
        let id_product = $(this).attr("id-product");

        for (let i=0;cart.length > i;i++){
          let item = cart[i];
          if(item.id == id_product){
            item.units = item.units - 1;
          }
        }        

        localStorage.setItem('cart',JSON.stringify(cart));
        
        Cart.createTable();
      });

      $(".btnDelete").on("click",function(){
        let cart = JSON.parse(localStorage.getItem('cart'));
        let id_product = $(this).attr("id-product");

        for (let i=0;cart.length > i;i++){
          let item = cart[i];
          if(item.id == id_product){
            cart.splice(i,1);
          }
        }        

        localStorage.setItem('cart',JSON.stringify(cart));
        
        Cart.createTable();
      });
    });
   
  </script>
  @endsection